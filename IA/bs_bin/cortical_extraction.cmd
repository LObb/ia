@setlocal enableextensions enabledelayedexpansion
@echo off

if "%~1"=="" (
	echo BrainSuite v18a Cortical Surface Extraction Script
  echo Authored by David W Shattuck http://shattuck.bmap.ucla.edu
  echo UCLA Brain Mapping Center
  echo For more information, please see: http://brainsuite.org
	echo usage: %~0 input_image
  echo where input_image is a raw MRI file, in .nii, .img, .nii.gz, or .img.gz format.
	exit /b 0
  )
echo BrainSuite v15c Cortical Surface Extraction Script
echo Authored by David W Shattuck http://shattuck.bmap.ucla.edu
echo UCLA Brain Mapping Center
echo for more information, please see: http://brainsuite.org

for %%? in ("%~dp0..") do set BrainSuitePath=%%~f?
set BrainSuiteBinDir="%~dp0"
if not exist "%BrainSuitePath%\bin\bse.exe" (
rem if you copy the script to another directory, you will want to set these locations to wherever BrainSuite is installed
echo could not find BrainSuite files... trying default install location C:\Program Files\BrainSuite18a\
set BrainSuitePath=C:\Program Files\BrainSuite18a
set BrainSuiteBinDir="C:\Program Files\BrainSuite18a\bin\"
)
if not exist "%BrainSuitePath%\bin\bse.exe" (
echo Could not find BrainSuite binaries. Quitting...
exit /b 1
)
echo ----------
echo BrainSuite is in %BrainSuitePath%\
echo Bin is %BrainSuiteBinDir%
echo ----------

echo Using BrainSuite binaries and data in %BrainSuitePath%

set basename=%1
rem remove any quotes from the string
set basename=%basename:"=%
rem remove img, img.gz, nii, nii.gz, hdr, etc.
if "x!basename:~-3!"=="x.gz" ( set basename=!basename:~0,-3!)
if "x!basename:~-4!"=="x.hdr" ( set basename=!basename:~0,-4!)
if "x!basename:~-4!"=="x.img" ( set basename=!basename:~0,-4!)
if "x!basename:~-4!"=="x.nii" ( set basename=!basename:~0,-4!)
rem add quotes in case there are spaces inside the string
set basename="%basename%"
rem change EXT to produce different outputs, e.g., uncompressed nifti
set EXT=nii.gz

echo working with basename: %basename%

@rem A few example variables that can be set. These settings match the ones in the GUI.
@rem use the VERBOSE flag to change the output option for all of the programs
@rem set VERBOSE=-v 1 --timer
@rem These values are set to match the defaults used in the GUI version of BrainSuite15b
set BSEOPTIONS=-n 3 -d 25 -s 0.64 -p --trim --auto
set BFCOPTIONS=-L 0.5 -U 1.5
@rem set PVCOPTIONS="-l 0.1"
set ATLAS=%BrainSuitePath%\atlas\brainsuite.icbm452.lpi.v08a.img
set ATLASLABELS=%BrainSuitePath%\atlas\brainsuite.icbm452.v15a.label.img
set ATLASES=--atlas "%ATLAS%" --atlaslabels "%ATLASLABELS%"
@rem set CEREBROOPTIONS="--centroids"

@rem you can specify an options file in the directory from which you launch the script
@rem Read from the BrainSuiteOptions file if it exists: strip out any double quotes and set variables accordingly
if exist BrainSuiteOptions (
echo Reading BrainSuite extraction settings from BrainSuiteOptions file
for /f "tokens=*" %%a in (BrainSuiteOptions) do (
	set z=%%a
	set z=!z:"=!
	echo !z!
	set !z!
)
)
echo ---- Running BrainSuite Cortical Surface Extraction Sequence ----
%BrainSuiteBinDir%bse %VERBOSE% -i %1 -o %basename%.bse.%EXT% --mask %basename%.mask.%EXT% %BSEOPTIONS%
if %ERRORLEVEL% NEQ 0 ( echo cortical extraction halted because BSE failed to run. && exit /b 1 )
%BrainSuiteBinDir%bfc %VERBOSE% -i %basename%.bse.%EXT% -o %basename%.bfc.%EXT% %BFCOPTIONS% %BFCFILES%
if %ERRORLEVEL% NEQ 0 ( echo cortical extraction halted because BSE failed to run. && exit /b 1 )
%BrainSuiteBinDir%pvc %VERBOSE% -i %basename%.bfc.%EXT% -o %basename%.pvc.label.%EXT% -f %basename%.pvc.frac.%EXT% %PVCOPTIONS%
if %ERRORLEVEL% NEQ 0 ( echo cortical extraction halted because bse failed to run. && exit /b 1 )
%BrainSuiteBinDir%cerebro %VERBOSE% %ATLASES% -i %basename%.bfc.%EXT% ^
	-l %basename%.hemi.label.%EXT% -m %basename%.mask.%EXT% -o %basename%.cerebrum.mask.%EXT% ^
	-c 2 --air %basename%.air --warp %basename%.warp %CEREBROOPTIONS%
if %ERRORLEVEL% NEQ 0 ( echo cortical extraction halted because cerebro failed to run. && exit /b 1 )
%BrainSuiteBinDir%cortex %VERBOSE% -f %basename%.pvc.frac.%EXT% -h %basename%.hemi.label.%EXT% -o %basename%.init.cortex.mask.%EXT% -a -w -p 50
if %ERRORLEVEL% NEQ 0 ( echo cortical extraction halted because cortex failed to run. && exit /b 1 )
%BrainSuiteBinDir%scrubmask %VERBOSE% -i %basename%.init.cortex.mask.%EXT% -o %basename%.cortex.scrubbed.mask.%EXT% -f 0 -b 2
if %ERRORLEVEL% NEQ 0 ( echo cortical extraction halted because scrubmask failed to run. && exit /b 1 )
%BrainSuiteBinDir%tca %VERBOSE% -i %basename%.cortex.scrubbed.mask.%EXT% -o %basename%.cortex.tca.mask.%EXT% -m 2500  --delta 20 %TCASETTINGS%
if %ERRORLEVEL% NEQ 0 ( echo cortical extraction halted because tca failed to run. && exit /b 1 )
%BrainSuiteBinDir%dewisp %VERBOSE% -i %basename%.cortex.tca.mask.%EXT% -o %basename%.cortex.dewisp.mask.%EXT% %DEWISPSETTINGS%
if %ERRORLEVEL% NEQ 0 ( echo cortical extraction halted because dewisp failed to run. && exit /b 1 )
rem %BrainSuiteBinDir%dfs %VERBOSE% -i %basename%.cortex.dewisp.mask.%EXT% -o %basename%.inner.cortex.rough.dfs -n 0
rem if %ERRORLEVEL% NEQ 0 ( echo cortical extraction halted because dfs failed to run. && exit /b 1 )
%BrainSuiteBinDir%dfs %VERBOSE% -i %basename%.cortex.dewisp.mask.%EXT% -o %basename%.inner.cortex.dfs -n 10 -a 0.5 -w 5.0
if %ERRORLEVEL% NEQ 0 ( echo cortical extraction halted because dfs failed to run. && exit /b 1 )
%BrainSuiteBinDir%pialmesh %VERBOSE% -f %basename%.pvc.frac.%EXT% -i %basename%.inner.cortex.dfs -m %basename%.cerebrum.mask.%EXT% -o %basename%.pial.cortex.dfs ^
	-n 100 -r 1 -s 0.40 -t 1.05 --max 20 --smooth 0.025 --interval 10 --nc 0.2
if %ERRORLEVEL% NEQ 0 ( echo cortical extraction halted because pialmesh failed to run. && exit /b 1 )
%BrainSuiteBinDir%hemisplit %VERBOSE% -i %basename%.inner.cortex.dfs -l %basename%.hemi.label.%EXT% ^
	--left %basename%.left.inner.cortex.dfs --right %basename%.right.inner.cortex.dfs ^
	-p %basename%.pial.cortex.dfs -pl %basename%.left.pial.cortex.dfs -pr %basename%.right.pial.cortex.dfs
if %ERRORLEVEL% NEQ 0 ( echo cortical extraction halted because hemisplit failed to run. && exit /b 1 )

exit /b 0

