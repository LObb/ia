function [pet, meanv] = maskedPET(labelim, petim, mask, label, T1, configFile)
%function [petstat, pet, petnames, meanv] = maskedPET(labelim, petim, mask, label, T1, configFile)

labelParser(label);

% load the configurations
selfName = self;
cfg= CfgRead(configFile)

% the intensity range of voxels to be used
perc = cfg.perc;
% load label and pet images
lim = GetTemporalMatrix('filematrix', labelim,  ':-', 3);
[pim, hdr] = GetTemporalMatrix('filematrix', petim,  ':-', 3);

% invert pet intensity if needed
if cfg.invpet
    pim = max(pim(:)) - pim;
end

% load mask image, and save the mask!
try
    mim = GetTemporalMatrix('filematrix',mask,  ':-', 3);
    mim = mim(end:-1:1,:,:);%%flipx
    rmim = zeros(size(mim));
    rmim((mim>cfg.threshold(1))) = 1;
    mim = rmim;
    hdr.fname = fullfile(fileparts(mask), 'Mask.nii');
    hdr = spm_create_vol(hdr);
    spm_write_vol(hdr, mim);
catch
    mim = 1;
end


% mask label and pet images
lim = lim.*mim;
pim = pim.*mim;

% save fmasked inverted PET
hdr2 = hdr;
file = petim
[filepath,name,ext] = fileparts(file)
%name
if cfg.invpet==0;
    fname=strcat('Segmented','_','Noninverted','_',name);
else;
    fname=strcat('Segmented','_','Inverted','_',name);
end;
%hdr2.fname = fullfile(fileparts(petim), 'SegmentedInvertedPET.nii');
hdr2.fname = fullfile(fileparts(petim), strcat(fname,ext));
hdr2 = spm_create_vol(hdr2);
spm_write_vol(hdr2, pim);

% merging all the labels in the list and relabel the label image
%% NEW in maskedPET3 and maskedPET4 %%
nschemes = size(cfg.mlists, 1);

for nsch = 1:nschemes
    % load labels
    load('label');
    %nlabels = numel(label);
    
    cfg.mlist = cfg.mlists(nsch,:);
    cfg.mgroups = cfg.mgroupss(nsch,:);

    nmergs = numel(cfg.mlist);
    for ims = 1:nmergs
        templist = cfg.mlist{ims};
        for ml = 2:numel(templist);
            lim(lim == templist(ml)) = templist(1);

            %      newlabel = [' + ' label{find(labelid==templist(ml))}];
        end
        %label{find(labelid==templist(1))} = cfg.mgroups{ims};
        %label{find(labelid==templist(1))} = [label{find(labelid==templist(1))} ' + ' newlabel];
        label{find(labelid==templist(1))} = cfg.mgroups{ims};
        if nsch>1
            labelr{ims} = cfg.mgroups{ims};
            labelidr(ims) = templist(1);
        end
    end
    if nsch>1
    label = labelr;
    labelid = labelidr;
    end
    nlabels = numel(label);
    
    CT = 1;
    totalv = 0;
    totaln = 0;
    gmn = 0;
    totalGMv = 0;
    totalGMn = 0;
    RgmMean=0;
    Asym=0;
    %z=0
    
    %% Filter the labels here: Items = (Items > 99 & Items < 502) | (Items > 1099 & Items < 1502);
    Items=labelid
    Filter = (labelid > 1099 & labelid < 4000)
    Items=Items(Filter)
    %nlabels=numel(Items)
    % loop through labels
    for i = 1:nlabels
        ind = (lim==labelid(i));
        %ind = (lim==Items(i));
        if (sum(ind(:))>5) && (sum((pim(ind))) > 0)
            %z=z+1;
            pet(CT).label = label{i};
            pet(CT).data = pim(ind);
            
            %% Updates to use only portions of the voxels per structure to calculate Asym
            tmpv = pim(ind);
            tmpv = sort(tmpv);
            tmpv = tmpv(round((perc(1))*numel(tmpv)):round((perc(2))*numel(tmpv)));
            
            % generate a matrix with mean, std and nvoxels, labelid, sum
            petstat(CT, 1) = mean(tmpv);
            petstat(CT, 2) = std(tmpv);
            petstat(CT, 3) = numel(tmpv);
            petstat(CT, 4) = labelid(i); %Items(i)
            petnames{CT} = label{i};
            
            %if ~isempty(strfind(petnames{CT},'(gm)'))
            if ~isempty(petnames{CT})
                gmn = gmn + 1;
                if strfind(petnames{CT},'R.') > 0
                    RgmMean = mean(tmpv);
                end
                if strfind(petnames{CT},'L.') > 0
                    Asym(CT,1) = CT;
                    Asym(CT,2) = RgmMean - mean(tmpv);
                    Asym(CT,3) = 2*((RgmMean - mean(tmpv))/(RgmMean + mean(tmpv)));
                end
                totalGMv = totalGMv + sum(pim(ind));
                totalGMn = totalGMn + sum(ind(:));
                %%%
            end
            
            totalv = totalv + sum(pim(ind));
            totaln = totaln + sum(ind(:));
            CT = CT + 1;
        end
    end
    
    %Calculate asymmetries
    %% Asym 4 & 5 are now reversed NEW in MaskedPET3+ %%
    for i=1:gmn 
        Asym(i,4)=Asym(i,2)/(totalGMv/totalGMn);
        Asym(i,5)=Asym(i,2)/(totalv/totaln);
    end
    
    % petnames = cellstr(petim); 
    %% NEW in maskedPET3+ %%
    meanv = totalv/totaln;
    meanGMv=totalGMv/totalGMn;
    
    pn = fileparts(petim);
    % save all the results
    save([pn, '/Summary.mat'], 'Asym', 'petstat', 'petnames','meanv' ,'meanGMv');
    
    if cfg.writexls
        %xlsn = fullfile(pn, 'Summary.xls');
        if cfg.invpet==0
            fname=strcat('Summary','_','Segmented','_','Noninverted','_',name,'.xls')
        else
            fname=strcat('Summary','_','Segmented','_','Inverted','_',name,'.xls')
        end
        %xlsn = fullfile(pn, ['Summary.xls']);
        xlsn = fullfile(pn, [fname]);
        %Write Excel sheet
        heads={'Label','Mean','Std','N voxels','LabelID','Asym','GM volume'};
        xlswrite(xlsn,heads,['Merge Scheme ' num2str(nsch)],'A1');
        xlswrite(xlsn,petstat,['Merge Scheme ' num2str(nsch)],'B2');
        xlswrite(xlsn,transpose(petnames),['Merge Scheme ' num2str(nsch)],'A2');
        xlswrite(xlsn,Asym,['Merge Scheme ' num2str(nsch)],'G2');
        asymv = abs(Asym(:,3));
        lids = (petstat(:,4)<=3000);
        asymv(lids) = 0;
        lbid = petstat(:,4);
        ids2extract = find(asymv>cfg.asym_threshold);
        
        for n2save = 1:numel(ids2extract)
            hdr2 = hdr;
            if cfg.invpet==0
                hdr2.fname = fullfile(fileparts(petim), [name,petnames{ids2extract(n2save)}(4:end), '.nii']);
            else
                hdr2.fname = fullfile(fileparts(petim),['i_',name,petnames{ids2extract(n2save)}(4:end), '.nii']);
            end
            %hdr2.fname = fullfile(fileparts(petim), [petnames{ids2extract(n2save)}(4:end), '.nii']);
            hdr2 = spm_create_vol(hdr2);%fails here n second pass
            npm = zeros(size(pim));
            npm(lim == lbid(ids2extract(n2save))) = pim(lim == lbid(ids2extract(n2save)));
            npm(lim == (lbid(ids2extract(n2save))-1)) = pim(lim == (lbid(ids2extract(n2save))-1));
            spm_write_vol(hdr2, npm);
        end
    end
    clear heads petstat petnames Asym;
end
%return